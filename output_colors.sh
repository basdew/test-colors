#!/bin/bash

# ”<Esc>[FormatCodem”.
# In Bash, the <Esc> character can be obtained with the following syntaxes:
# \e
# \033
# \x1B

# The ”\e[0m” sequence removes all attributes (formatting and colors). It can be a good idea to add it at the end of each colored text. ;)

c_def="\033[0m"
c_dim="\033[2m"
c_gray="\033[30m"
c_red="\033[91m"
c_yellow="\033[93m"
c_ltblue="\033[96m"
c_green="\033[32m"
c_blue="\033[34m"
c_ul="\033[4m"


echo -e "${c_red}This is supposed to be red.\n${c_def}"
echo -e "${c_dim}This is supposed to be dim.\n${c_def}"
echo -e "${c_gray}This is supposed to be gray.\n${c_def}"
echo -e "${c_red}This is supposed to be red.\n${c_def}"
echo -e "${c_yellow}This is supposed to be yellow.\n${c_def}"
echo -e "${c_ltblue}This is supposed to be ltblue.\n${c_def}"
echo -e "${c_green}This is supposed to be green.\n${c_def}"
echo -e "${c_blue}This is supposed to be blue.\n${c_def}"
echo -e "${c_ul}This is supposed to be underlined.\n${c_def}"


# ------------------------------------------------------------------------------------------------------------------
 
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.
 
#Background
for clbg in {40..47} {100..107} 49 ; do
	#Foreground
	for clfg in {30..37} {90..97} 39 ; do
		#Formatting
		for attr in 0 1 2 4 5 7 ; do
			#Print the result
			echo -en "\e[${attr};${clbg};${clfg}m ^[${attr};${clbg};${clfg}m \e[0m"
		done
		echo #Newline
	done
done
 
exit 0