'use strict';

var clc = require('cli-color');
var setupThrobber = require('cli-color/throbber');

console.log('Output colored text:');
console.log(clc.red('Text in red'));

console.log('Styles can be mixed:');
console.log(clc.red.bgWhite.underline('Underlined red text on white background.'));

console.log('Styled text can be mixed with unstyled:');
console.log(clc.red('red') + ' plain ' + clc.blue('blue'));

console.log('Styled text can be nested:');
console.log(clc.red('red ' + clc.blue('blue') + ' red'));

console.log('Best way is to predefine needed stylings and then use it:');

var error = clc.red.bold;
var warn = clc.yellow;
var notice = clc.blue;

console.log(error('Error!'));
console.log(warn('Warning'));
console.log(notice('Notice'));


console.log("=======================================================================");

console.log(clc.bold('Bold text'));
console.log(clc.italic('Italic text'));
console.log(clc.underline('Underlined text'));
console.log(clc.blink('Blinking text (might not work for your font)'));
console.log(clc.inverse('Inverse text'));
console.log(clc.strike('Strikethrough text'));


console.log("=======================================================================");



var throbber = setupThrobber(function (str) {
	process.stdout.write(str);
}, 200);

process.stdout.write('Throbbing for 3 seconds here -> ');
throbber.start();

setTimeout(function () {
	console.log();
	throbber.stop();
}, 3000);

console.log("=======================================================================");

var msg = clc.xterm(202).bgXterm(236);
console.log(msg('Orange text on dark gray background'));

console.log(clc.bgBlue.whiteBright('white bright on blue background'));
console.log(clc.bgRedBright.white('white on red background'));
console.log(clc.green('green text'));